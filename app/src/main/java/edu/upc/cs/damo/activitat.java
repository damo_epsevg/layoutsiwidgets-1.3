// 1.3

// Un text fix i un text editable
// Cal introduir el LinearLayout

// Posem els literals com a constants per tal de millorar la legibilitat



package edu.upc.cs.damo;

import android.app.Activity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;


public class activitat extends Activity {
    /** Called when the activity is first created. */

    static String SALUTACIO = "Hola a tothom";
    TextView text;
    EditText camp;
    LinearLayout layout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Construim els elements a visualitzar

        text = new TextView(this);
        text.setText(SALUTACIO);

        camp = new EditText(this);

        // Introduim els elements en un contenidor
        layout = new LinearLayout(this);
        layout.addView(text);
        layout.addView(camp);
        setContentView(layout);
    }
}
